﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using Microsoft.Win32.SafeHandles;

namespace FAT_driver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter device or image path:");
            FAT currentFAT = new FAT();
            StreamWriter sw = new StreamWriter("output.txt");
            //reflection
            FieldInfo[] fields = currentFAT.GetType().GetFields(BindingFlags.Public |
                                              BindingFlags.NonPublic |
                                              BindingFlags.Instance);
            StringBuilder str = new StringBuilder();
            foreach (FieldInfo f in fields)
            {
                sw.WriteLine(f.Name + " = " + f.GetValue(currentFAT) + "\r\n");
            }
            sw.Close();
        }
    }

    internal class FAT
    {

        #region CreateFile @ kernel32.dll

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall,
            SetLastError = true)]
        private static extern SafeFileHandle CreateFile(
            string lpFileName,
            uint dwDesiredAccess,
            uint dwShareMode,
            IntPtr lpSecurityAttributes,
            uint dwCreationDisposition,
            uint dwFlagsAndAttributes,
            IntPtr hTemplateFile);

        [Flags]
        private enum EFileAccess : uint
        {
            AccessSystemSecurity = 0x1000000,
            MaximumAllowed = 0x2000000,

            Delete = 0x10000,
            ReadControl = 0x20000,
            WriteDAC = 0x40000,
            WriteOwner = 0x80000,
            Synchronize = 0x100000,

            StandardRightsRequired = 0xF0000,
            StandardRightsRead = ReadControl,
            StandardRightsWrite = ReadControl,
            StandardRightsExecute = ReadControl,
            StandardRightsAll = 0x1F0000,
            SpecificRightsAll = 0xFFFF,

            FILE_READ_DATA = 0x0001,
            FILE_LIST_DIRECTORY = 0x0001,
            FILE_WRITE_DATA = 0x0002,
            FILE_ADD_FILE = 0x0002,
            FILE_APPEND_DATA = 0x0004,
            FILE_ADD_SUBDIRECTORY = 0x0004,
            FILE_CREATE_PIPE_INSTANCE = 0x0004,
            FILE_READ_EA = 0x0008,
            FILE_WRITE_EA = 0x0010,
            FILE_EXECUTE = 0x0020,
            FILE_TRAVERSE = 0x0020,
            FILE_DELETE_CHILD = 0x0040,
            FILE_READ_ATTRIBUTES = 0x0080,
            FILE_WRITE_ATTRIBUTES = 0x0100,

            GenericRead = 0x80000000,
            GenericWrite = 0x40000000,
            GenericExecute = 0x20000000,
            GenericAll = 0x10000000,

            SPECIFIC_RIGHTS_ALL = 0x00FFFF,

            FILE_ALL_ACCESS =
                StandardRightsRequired |
                Synchronize |
                0x1FF,

            FILE_GENERIC_READ =
                StandardRightsRead |
                FILE_READ_DATA |
                FILE_READ_ATTRIBUTES |
                FILE_READ_EA |
                Synchronize,

            FILE_GENERIC_WRITE =
                StandardRightsWrite |
                FILE_WRITE_DATA |
                FILE_WRITE_ATTRIBUTES |
                FILE_WRITE_EA |
                FILE_APPEND_DATA |
                Synchronize,

            FILE_GENERIC_EXECUTE =
                StandardRightsExecute |
                FILE_READ_ATTRIBUTES |
                FILE_EXECUTE |
                Synchronize
        }

        [Flags]
        public enum EFileShare : uint
        {
            None = 0x00000000,
            Read = 0x00000001,
            Write = 0x00000002,
            Delete = 0x00000004
        }

        public enum ECreationDisposition : uint
        {
            New = 1,
            CreateAlways = 2,
            OpenExisting = 3,
            OpenAlways = 4,
            TruncateExisting = 5
        }

        [Flags]
        public enum EFileAttributes : uint
        {
            Readonly = 0x00000001,
            Hidden = 0x00000002,
            System = 0x00000004,
            Directory = 0x00000010,
            Archive = 0x00000020,
            Device = 0x00000040,
            Normal = 0x00000080,
            Temporary = 0x00000100,
            SparseFile = 0x00000200,
            ReparsePolong = 0x00000400,
            Compressed = 0x00000800,
            Offline = 0x00001000,
            NotContentIndexed = 0x00002000,
            Encrypted = 0x00004000,
            Write_Through = 0x80000000,
            Overlapped = 0x40000000,
            NoBuffering = 0x20000000,
            RandomAccess = 0x10000000,
            SequentialScan = 0x08000000,
            DeleteOnClose = 0x04000000,
            BackupSemantics = 0x02000000,
            PosixSemantics = 0x01000000,
            OpenReparsePolong = 0x00200000,
            OpenNoRecall = 0x00100000,
            FirstPipeInstance = 0x00080000
        }

        #endregion

        #region File reading


        private static SafeFileHandle fileHandle = CreateFile(
            Console.ReadLine(),
            (uint)EFileAccess.GenericRead,
            (uint)EFileShare.Delete,
            IntPtr.Zero,
            (uint)ECreationDisposition.OpenAlways,
            (uint)EFileAttributes.Device,
            IntPtr.Zero);

        private static FileStream fstream = new FileStream(fileHandle, FileAccess.Read);
        public static BinaryReader Reader = new BinaryReader(fstream);

        private static string ReadString(byte[] stringBytes)
        {
            return Encoding.GetEncoding(866).GetString(stringBytes);
        }

        private static byte[] ReadDirectory()
        {
            return Reader.ReadBytes(32);
        }

        private static string ReadLongDirectoryName(List<byte[]> directory)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = directory.Count - 1; i >= 0; i--)
            {
                for (int j = 1; j < 11; j += 2)
                    if (directory[i][j] != 0x0000 && directory[i][j] != 255)
                        sb.Append(Encoding.Unicode.GetString(directory[i], j, 2));
                for (int j = 14; j < 26; j += 2)
                    if (directory[i][j] != 0x0000 && directory[i][j] != 255)
                        sb.Append(Encoding.Unicode.GetString(directory[i], j, 2));
                for (int j = 28; j < 32; j += 2)
                    if (directory[i][j] != 0x0000 && directory[i][j] != 255)
                        sb.Append(Encoding.Unicode.GetString(directory[i], j, 2));
            }
            return sb.ToString();
        }

        private static char ReadDirectoryNameSymbol(byte[] namePiece, long currentIndex)
        {
            long result = 0;
            int baseS = 0;
            for (long i = currentIndex; i >= currentIndex - 1; i--, baseS += 8)
                result += namePiece[i] << baseS;
            return (char)result;
        }

        private static long ReadNumber(byte[] numberBytes)
        {
            long result = 0;
            int baseS = 0;
            for (int i = 0; i < numberBytes.Length; i++, baseS += 8)
                result += (long)numberBytes[i] << baseS;
            return result;
        }   

        void BrowseDirectory(string userRequest)
        {
            if (!userRequest.Equals("Q", StringComparison.InvariantCultureIgnoreCase))
            {
                try
                {
                    int directoryNumber = int.Parse(userRequest);
                    if (address[directoryNumber].Address < 0)
                    {
                        ExtractFile(userRequest);
                        Console.WriteLine("Enter the number of directory to move to or the number of file to extract. 'Q' to quit");
                        BrowseDirectory(Console.ReadLine());
                    } //EXTRACT
                    else
                    {
                        PrintDirectory(address[directoryNumber].Address);
                        Console.WriteLine("Enter the number of directory to move to or the number of file to extract. 'Q' to quit");
                        BrowseDirectory(Console.ReadLine());
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Enter correct number:");
                    BrowseDirectory(Console.ReadLine());
                }
            }
        }

        #endregion

        #region ExtractFile

        void ExtractFile(string userRequest)
        {
            try
            {
                int fileNumber = int.Parse(userRequest);
                int[] clusters = new int[address[fileNumber].SizeOfFile / BPB_BytsPerSec / BPB_SecPerClus + 1];
                clusters[0] = (int)-address[fileNumber].Address;
                for (int i = 1; i < clusters.Length; i++)
                    clusters[i] = (int)GetNextCluster(clusters[i - 1]);
                Console.WriteLine("Enter path to extract file to");
                BinaryWriter bw = new BinaryWriter(File.OpenWrite(Console.ReadLine()));
                long position = 0;
                for (int i = 0; i < clusters.Length; i++)
                {
                    long sector = ((clusters[i] - 2) * BPB_SecPerClus) + FirstDataSector;
                   Seek(sector * BPB_BytsPerSec);
                    byte[] buffer = Reader.ReadBytes((int)(BPB_BytsPerSec * BPB_SecPerClus));
                    if (position + BPB_BytsPerSec * BPB_SecPerClus < address[fileNumber].SizeOfFile)
                        bw.Write(buffer);
                    else
                        bw.Write(buffer, 0, (int)(address[fileNumber].SizeOfFile - position));
                    position += BPB_BytsPerSec * BPB_SecPerClus;

                }
                bw.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Could not extract file in specified path");
            }
        }

        #endregion

        #region Data Service

        private long BS_jmpBoot = ReadNumber(Reader.ReadBytes(3));
        private string BS_OEMName = ReadString(Reader.ReadBytes(8));
        private long BPB_BytsPerSec = ReadNumber(Reader.ReadBytes(2)); //BytesToLong(Reader.ReadBytes(2), 0, 2, true);ReadNumber(Reader.ReadBytes(2));// only 512, 1024, 2048 and 4096 are correct, but 512 is the best for compatibility
        private long BPB_SecPerClus = ReadNumber(Reader.ReadBytes(1));
        private long BPB_RsvdSecCnt = ReadNumber(Reader.ReadBytes(2));
        private long BPB_NumFATs = ReadNumber(Reader.ReadBytes(1));// 2 is recommended
        private long BPB_RootEntCnt = ReadNumber(Reader.ReadBytes(2));
        private long BPB_TotSec16 = ReadNumber(Reader.ReadBytes(2));
        private long BPB_Media = ReadNumber(Reader.ReadBytes(1));
        private long BPB_FATSz16 = ReadNumber(Reader.ReadBytes(2));
        private long BPB_SecPerTrk = ReadNumber(Reader.ReadBytes(2));
        private long BPB_NumHeads = ReadNumber(Reader.ReadBytes(2));
        private long BPB_HiddSec = ReadNumber(Reader.ReadBytes(4));
        private long BPB_TotSec32 = ReadNumber(Reader.ReadBytes(4));

        //common for FAT12, FAT16 and FAT32, but it has different offset in FAT32 and elder versions
        private long BS_DrvNum;
        private long BS_BootSig;
        private long BS_VolID;
        private string BS_VolLab;
        private string BS_FilSysType;

        //Fat32 Structure Starting at Offset 36
        private long BPB_FATSz32;
        private long BPB_ExtFlags;
        private long BPB_FSVer;
        private long BPB_RootClus;
        private long BPB_FSInfo;
        private long BPB_BkBootSec;

        //FAT32 FSInfo Sector Structure and Backup Boot sector
        private long FSI_LeadSig;
        private long FSI_StrucSig;
        private long FSI_Free_Count;
        private long FSI_Nxt_Free;
        private long FSI_TrailSig;

        private long FirstDataSector;//for reading

        class Entry
        {
            public long Address;
            public long SizeOfFile;

            public Entry(long address,long sizeOfFile)
            {
                Address = address;
                SizeOfFile = sizeOfFile;
            }
        }

        List<Entry> address = new List<Entry>();

        private int version;

        #endregion


        #region FAT version

        private int FAT_version()
        {
            double aux = (double)BPB_BytsPerSec;
            aux = (aux - 1) / aux;
            if (aux - (long)aux != 0)
                aux++;
            long RootDirSectors = (BPB_RootEntCnt * 32) + (long)(aux);
            long FATSz;
            long TotSec;
            if (BPB_FATSz16 != 0)
                FATSz = BPB_FATSz16;
            else
                FATSz = BPB_FATSz32;

            if (BPB_TotSec16 != 0)
                TotSec = BPB_TotSec16;
            else
                TotSec = BPB_TotSec32;

            long DataSec = TotSec - (BPB_RsvdSecCnt + (BPB_NumFATs * FATSz) + RootDirSectors);
            long CountofClusters = DataSec / BPB_SecPerClus;
            if (CountofClusters < 4085)
            {
                return 12;
            }
            else if (CountofClusters < 65525)
            {
                return 16;
            }
            else
            {
                return 32;
            }
        }

        #endregion

        private void PrintDirectory(long firstCluster)
        {
            List<int> clusters = new List<int>();
            while (firstCluster != (version == 32 ? 0x0FFFFFFF : 0xFFFF) && firstCluster != 0)
            {
                clusters.Add((int)firstCluster);
                long sector = version != 32 ? BPB_RsvdSecCnt + BPB_NumFATs * BPB_FATSz16 : ((firstCluster - 2) * BPB_SecPerClus) + FirstDataSector;
                Seek(sector * BPB_BytsPerSec);
                firstCluster = GetNextCluster((int)firstCluster);
            }
            List<Entry> addressLocal = new List<Entry>();

            byte[] currentDirectory = ReadDirectory(); // 32 bytes
            foreach (var cluster in clusters)
            {
                long currentDirectorySector = (cluster - 2) * BPB_SecPerClus + FirstDataSector;
                Seek(currentDirectorySector * BPB_BytsPerSec); //we are at the beginning of the root directory now
                long pos = 0;
                int numberDirectories = 0;
                while (currentDirectory[0] != 0x0 && numberDirectories < BPB_SecPerClus * BPB_BytsPerSec / 32)
                {
                    numberDirectories++;
                    if (currentDirectory[0] == 0xE5)
                    {
                        currentDirectory = ReadDirectory();
                        continue;
                    }
                    List<byte[]> longNameParts = new List<byte[]>();
                    string directoryName = "";
                    bool longDirectory = false;
                    if (currentDirectory[11] == 0x0F) // long directory
                    {
                        longDirectory = true;
                        while (currentDirectory[11] == 0x0F)
                        {
                            longNameParts.Add(currentDirectory);
                            currentDirectory = ReadDirectory();
                        }
                        directoryName = ReadLongDirectoryName(longNameParts);
                    }
                    if ((char)currentDirectory[0] == '.')
                        if ((char)currentDirectory[1] == '.') ;
                        //directory.Add(currentDirectory);
                        else
                        {
                            currentDirectory = ReadDirectory();
                            continue;
                        }
                    byte[] byteAddress = new byte[4];
                    byteAddress[0] = currentDirectory[26];
                    byteAddress[1] = currentDirectory[27];
                    byteAddress[2] = currentDirectory[20];
                    byteAddress[3] = currentDirectory[21];
                    if (!longDirectory)
                    {
                        byte[] byteName = new byte[11];
                        for (long i = 0; i < byteName.Length; i++)
                            byteName[i] = currentDirectory[i];
                        directoryName = ReadString(byteName);
                    }
                    byte[] byteFileSize = new byte[4];
                    for (long i = 0; i < 4; i++)
                        byteFileSize[i] = currentDirectory[28 + i];
                    if (currentDirectory[11] != 0x08)
                        Console.WriteLine("{0,4} {1,12}  {2}", addressLocal.Count,
                           (currentDirectory[11] & 0x10) == 0x10 ? "<DIR>" : ReadNumber(byteFileSize).ToString(),
                            directoryName);
                    long childFirstCluster = ReadNumber(byteAddress);
                    if ((currentDirectory[11] & 0x10) == 0x10)
                        addressLocal.Add(new Entry(childFirstCluster == 0 ? 2 :childFirstCluster,0));
                    else
                        addressLocal.Add(new Entry(-childFirstCluster, ReadNumber(byteFileSize)));
                    currentDirectory = ReadDirectory();
                }
            }
            address = addressLocal;
            /*  if ( == 0)
                  Console.WriteLine("This directory is empty.");*/
        }

        public FAT()
        {
            if (fileHandle.IsInvalid)
            {
                Console.WriteLine("You entered wrong address. This directory can not be opened.");

            }
            else
            {
                version = FAT_version();
                if (version == 32)
                {
                    BPB_FATSz32 = ReadNumber(Reader.ReadBytes(4));
                    BPB_ExtFlags = ReadNumber(Reader.ReadBytes(2));
                    BPB_FSVer = ReadNumber(Reader.ReadBytes(2));
                    BPB_RootClus = ReadNumber(Reader.ReadBytes(4));
                    BPB_FSInfo = ReadNumber(Reader.ReadBytes(2));
                    BPB_BkBootSec = ReadNumber(Reader.ReadBytes(2));
                    Seek(64); //BPB_Reserved skeeped

                }
                BS_DrvNum = ReadNumber(Reader.ReadBytes(1));
                if (version == 32)
                    Seek(66); //BPB_Reserved1 skeeped
                else
                    Seek(8192); //BPB_Reserved1 skeeped
                BS_BootSig = ReadNumber(Reader.ReadBytes(1));
                BS_VolID = ReadNumber(Reader.ReadBytes(4));
                BS_VolLab = ReadString(Reader.ReadBytes(11));
                BS_FilSysType = ReadString(Reader.ReadBytes(8));
                if (version == 32) //FAT32 FSInfo Sector Structure and Backup Boot sector
                {
                    Seek((BPB_FSInfo + 1) * BPB_BytsPerSec);
                    FSI_LeadSig = ReadNumber(Reader.ReadBytes(4));
                    Seek((BPB_FSInfo + 1) * BPB_BytsPerSec + 484); //FSI_Reserved1 skeeped
                    FSI_StrucSig = ReadNumber(Reader.ReadBytes(4));
                    FSI_Free_Count = ReadNumber(Reader.ReadBytes(4));
                    FSI_Nxt_Free = ReadNumber(Reader.ReadBytes(4));
                    Seek((BPB_FSInfo + 1) * BPB_BytsPerSec + 508); //FSI_Reserved2 skeeped
                }
                if (version == 32)
                    FirstDataSector = BPB_RsvdSecCnt + (BPB_NumFATs * BPB_FATSz32) + BPB_RootEntCnt * 32 / BPB_BytsPerSec;
                else
                    FirstDataSector = BPB_RsvdSecCnt + (BPB_NumFATs * BPB_FATSz16) + BPB_RootEntCnt * 32 / BPB_BytsPerSec;
                PrintDirectory(version == 32 ? BPB_RootClus : 2 + (BPB_RootEntCnt * 32) / BPB_SecPerClus);
                Console.WriteLine("Enter the number of directory to move to or the number of file to extract. 'Q' to quit");
                BrowseDirectory(Console.ReadLine());
            }
        }

        private void Seek(long position)
        {
            long realPos = position / BPB_BytsPerSec * BPB_BytsPerSec;
            long shift = position - realPos;
            fstream.Seek(realPos, SeekOrigin.Begin);
            for (long i = 0; i < shift; i++)
                fstream.ReadByte();
        }

        private long GetNextCluster(int N)
        {
            long FATSz;
            int FATOffset = 0;
            long ThisFATSecNum;
            int ThisFATEntOffset;
            if (BPB_FATSz16 != 0)
                FATSz = BPB_FATSz16;
            else FATSz = BPB_FATSz32;
            if (version == 16) FATOffset = N * 2;
            else if (version == 32) FATOffset = N * 4;
            ThisFATSecNum = BPB_RsvdSecCnt + (FATOffset / BPB_BytsPerSec);
            ThisFATEntOffset = (int)(FATOffset % BPB_BytsPerSec);
            long last_pos = fstream.Position;
            Seek(ThisFATSecNum * BPB_BytsPerSec);
            Reader.ReadBytes(ThisFATEntOffset); //пропустить entry_offset
            long result = ReadNumber(Reader.ReadBytes(version / 8));
            Seek(last_pos);
            return result;
        }

    }
}